const express = require('express');
const router = express.Router();

//import
const productController = require('../controllers/productController');
 

//Route for getting all products
router.get('/', (req, res)=>{
	//Get all products
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
})

//Route for getting a specific id
router.get('/:id', (req, res)=>{
	//Get single product
	productController.getProduct().then(resultFromController => res.send(resultFromController))
})

//Route to add a new product
router.post('/', (req, res)=>{
	//Add a new product
	productController.createProduct(req.body).then(resultFromController => res.send(resultFromController))
})

//Route to delete a product

router.delete('/delete/:id', (req, res)=>{
	//Delete Product
	productController.deleteProduct().then(resultFromController => res.send(resultFromController))
})


//module exports
module.exports = router;