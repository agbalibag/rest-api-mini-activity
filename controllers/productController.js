//Model
const Product = require('../models/product')

//Get all products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		 return result;
	})
}

//Get a single product
module.exports.getProduct = (productId) => {
	return Product.findById(productId).then((result, error) => {
		if(error){
			console.log(error)
			return false;
		}else{
			return result;
		}
	})
}

//Add a new product
module.exports.createProduct = (requestBody) => {
	let newProduct = new Product({
		name: requestBody.name,
		price: requestBody.price
	})
	return newProduct.save().then((product, error) => {
		if(error){
			console.log(error)
		}else{
			return product;
		}
	})
}

//Delete a product
module.exports.deleteProduct  = (productId) => {
	return Product.findByIdAndRemove(productId).then((removedProduct, err) => {
		if(err){
			console.log(err)
			return false;
		}else{
			return removedProduct;

		}
	})
}