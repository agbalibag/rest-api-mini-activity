const express = require('express');
const mongoose = require('mongoose');

const productRoute = require('./routes/productRoute')

const app = express();
const port = 3002;

app.use(express.json());
app.use(express.urlencoded({ extended:true }))

//Connect to MongoDB
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.upmgf.mongodb.net/Rest-API-mini-activity?retryWrites=true&w=majority',
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});


app.use('/products', productRoute)


//Server listening
app.listen(port, ()=>console.log(`Listening to port ${port}`));
